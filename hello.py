#!/user/bin/env python3
"""
[Add module documentation here]

Author: Fortinux
Date: [20200906]
"""
from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def hello_world():
#    return "<h1 style='color:green'>Hello World!</h1>"
    return render_template('index.html')
